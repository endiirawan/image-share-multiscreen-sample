package com.srin.imagesharesample.tools;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.samsung.multiscreen.device.Device;

public class DevicesAdapter extends ArrayAdapter<Device> {
    private List<Device> mDevices;
    private LayoutInflater inflater;

    public void setDevices(List<Device> devices) {
        this.mDevices = devices;
    }

    public DevicesAdapter(Context context, List<Device> devices) {
        super(context, android.R.layout.simple_spinner_dropdown_item, devices);
        this.mDevices = devices;
        inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        Device device = (Device) mDevices.get(position);

        TextView textView = (TextView) row;
        textView.setText(device.getName() + " - " + device.getIPAddress());
        return row;
    }

}