package com.srin.imagesharesample.tools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

public class GalleryAdapter {
    public static final String CAMERA_IMAGE_BUCKET_NAME = Environment.getExternalStorageDirectory().toString() + "/DCIM/Camera";
    public static final String CAMERA_IMAGE_BUCKET_ID = getBucketId(CAMERA_IMAGE_BUCKET_NAME);

    /**
     * Matches code in MediaProvider.computeBucketValues. Should be a common
     * function.
     */
    public static String getBucketId(String path) {
        return String.valueOf(path.toLowerCase().hashCode());
    }

    public static List<String> getCameraImages(Context context) {
        int limit = 10;
        final String[] projection = { MediaStore.Images.Media.DATA };
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = { CAMERA_IMAGE_BUCKET_ID };
        final Cursor cursor = context.getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            selection,
            selectionArgs,
            null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            int i = 1;
            do {
                final String data = cursor.getString(dataColumn);
                result.add(data);
                i++;
            } while (cursor.moveToNext() && i < limit);
        }
        cursor.close();
        return result;
    }

    public static ImageView createImageView(Context context, String imagePath) {
        File imgFile = new File(imagePath);
        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            myBitmap = Bitmap.createScaledBitmap(myBitmap, 160, 160, true);

            ImageView iv = new ImageView(context);
            LayoutParams lp = new LayoutParams(300, LayoutParams.MATCH_PARENT);
            lp.width = 300;
            lp.height = 300;
            lp.rightMargin = 5;
            iv.setLayoutParams(lp);
            iv.setTag(imagePath);
            iv.setImageBitmap(myBitmap);
            return iv;
        }
        return null;
    }

    public static String getEncodedImage(String filepath) {
        Bitmap bm = BitmapFactory.decodeFile(filepath);
        bm = Bitmap.createScaledBitmap(bm, 160, 160, true);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();

        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }
}
