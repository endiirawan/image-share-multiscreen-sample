package com.srin.imagesharesample;

import com.samsung.multiscreen.channel.Channel;
import com.samsung.multiscreen.device.Device;

public class DataManager {
    // APP ID
    public static final String CHANNEL_ID = "com.samsung.multiscreen.chatbinus";
    // RUN TITLE
    public static final String RUN_TITLE = "ChatBinus";

    // SELECTED DEVICE
    public static Device mDevice;
    // CURRENT CHANNEL
    public static Channel mChannel;

}
