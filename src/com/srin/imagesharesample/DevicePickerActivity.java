package com.srin.imagesharesample;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.samsung.multiscreen.device.Device;
import com.samsung.multiscreen.device.DeviceAsyncResult;
import com.samsung.multiscreen.device.DeviceError;
import com.srin.imagesharesample.tools.DevicesAdapter;

public class DevicePickerActivity extends Activity implements OnClickListener, OnItemClickListener {
    private static final String LOGTAG = DevicePickerActivity.class.getSimpleName();
    private ProgressDialog progressDialog;
    private ListView devicesListView;
    private EditText mPinText;
    public static final String EXTRA_SELECTED_DEVICE_URI = "selected_dev_id";
    private List<Device> deviceLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // SETUP VIEW
        Button findLocalButton = (Button) findViewById(R.id.findLocal);
        Button getDeviceButton = (Button) findViewById(R.id.btn_get_device);
        findLocalButton.setOnClickListener(this);
        getDeviceButton.setOnClickListener(this);
        this.devicesListView = (ListView) findViewById(R.id.listDevices);
        this.devicesListView.setOnItemClickListener(this);
        this.mPinText = (EditText) findViewById(R.id.pinCodeEntry);

        // SETUP ADAPTER
        this.deviceLists = new ArrayList<Device>();
        DevicesAdapter devicesAdapter = new DevicesAdapter(DevicePickerActivity.this, deviceLists);
        devicesListView.setAdapter(devicesAdapter);
    }

    private void showProgress(String message) {
        hideProgress();
        progressDialog = ProgressDialog.show(this, "", message, true);
    }

    private void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void getByCode() {
        showProgress("Getting Device..");
        String pinCode = mPinText.getText().toString();
        Device.getByCode(pinCode, new DeviceAsyncResult<Device>() {
            @Override
            public void onResult(final Device device) {
                hideProgress();
                // Device Found
                Log.d("get_by_code", String.format("Device Name : %s", device.getName()));
                Log.d("search", String.format("Service URI : %s", device.getServiceURI()));

                selectDevice(device);
            }

            @Override
            public void onError(final DeviceError error) {
                // Device Not Found
                hideProgress();
                Log.e("search", "Search Failed : " + error.getMessage());
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(DevicePickerActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ;
            }
        });
    }

    /**
     * MAIN METHOD FOR FINDING LOCAL DEVICES
     */
    private void findLocalDevices() {
        Log.d(LOGTAG, "findLocalDevices()");

        showProgress("Searching....");

        Device.search(new DeviceAsyncResult<List<Device>>() {
            @Override
            public void onResult(final List<Device> devices) {
                Log.d("search", "Search Result : " + devices.size());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        // Init the list with devices found
                        deviceLists = devices;
                        DevicesAdapter devicesAdapter = new DevicesAdapter(DevicePickerActivity.this, deviceLists);
                        devicesListView.setAdapter(devicesAdapter);
                    }
                });
            }

            @Override
            public void onError(final DeviceError error) {
                Log.e("search", "Search Failed : " + error.getMessage());

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        hideProgress();
                        Toast.makeText(DevicePickerActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void selectDevice(Device device) {
        Intent data = new Intent(this, AppLauncherActivity.class);
        data.putExtra(EXTRA_SELECTED_DEVICE_URI, device.getServiceURI().toString());
        Log.d(LOGTAG, "URI: " + data.getStringExtra(EXTRA_SELECTED_DEVICE_URI));
        startActivity(data);
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.findLocal:
                findLocalDevices();
                break;
            case R.id.btn_get_device:
                getByCode();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        Device device = (Device) devicesListView.getItemAtPosition(position);
        Log.d(LOGTAG, "devicesList.onItemClick() device: " + device);
        if (device != null) {
            selectDevice(device);
        }
    }
}
