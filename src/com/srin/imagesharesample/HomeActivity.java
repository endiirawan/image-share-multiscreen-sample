package com.srin.imagesharesample;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.samsung.multiscreen.channel.ChannelAsyncResult;
import com.samsung.multiscreen.channel.ChannelError;
import com.srin.imagesharesample.tools.GalleryAdapter;

public class HomeActivity extends Activity implements OnClickListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private ProgressDialog progressDialog;
    private LinearLayout mTVLogo;
    private LinearLayout mImageContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.mImageContainer = (LinearLayout) findViewById(R.id.image_container);
        this.mTVLogo = (LinearLayout) findViewById(R.id.tvLogo);
        this.mTVLogo.setOnDragListener(new ImageDragListener());
        this.mTVLogo.setOnClickListener(this);

        loadGalleryImages();
    }

    private void loadGalleryImages() {
        new AsyncTask<Void, ImageView, Void>() {
            protected void onPreExecute() {
                showProgressDialog("Loading Gallery Images");
            };

            @Override
            protected Void doInBackground(Void[] params) {
                List<String> images = GalleryAdapter.getCameraImages(HomeActivity.this);
                for (String string : images) {
                    ImageView iv = GalleryAdapter.createImageView(HomeActivity.this, string);
                    if (null != iv) {
                        publishProgress(iv);
                    }
                }
                return null;
            };

            protected void onProgressUpdate(ImageView[] values) {
                ImageView iv = values[0];
                if (null != iv) {
                    iv.setOnLongClickListener(new LongClickListener());
                    mImageContainer.addView(iv);
                }

            };

            protected void onPostExecute(Void result) {
                hideProgressDialog();
            };
        }.execute();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Disconnect ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        disconnectChannel();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                    }
                })
                .show();
        }
        return true;
    }

    private void showProgressDialog(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                progressDialog = ProgressDialog.show(HomeActivity.this, "", message, true);
            }
        });
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * Disconnect From Channel
     */
    private void disconnectChannel() {
        if (null == DataManager.mChannel) {
            return;
        }
        showProgressDialog("Disconnecting..");

        // TODO: DISCONNECT
        DataManager.mChannel.disconnect(new ChannelAsyncResult<Boolean>() {

            @Override
            public void onResult(Boolean status) {
                DataManager.mChannel = null;
                hideProgressDialog();
                finish();
                
             
            }

            @Override
            public void onError(ChannelError e) {
                hideProgressDialog();
                Toast.makeText(HomeActivity.this, "Failed : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Sends a chat message to the selected recipient
     */
    private void sendEncodedImage(final String message) {
        if (message.equals("") || DataManager.mChannel == null) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {
                showProgressDialog("Sending Image");
            };

            protected Void doInBackground(Void[] params) {
                boolean encryptMessage = true;
                JSONObject jsonMessage = new JSONObject();
                try {
                    jsonMessage.put("type", "sendimage");
                    jsonMessage.put("text", "data:image/bmp;base64," + message);
                    String messageText = jsonMessage.toString();
                    DataManager.mChannel.sendToHost(messageText, encryptMessage);
                    Log.d(TAG, "IMAGE SENT");
                } catch (JSONException e) {
                    Log.d(TAG, "Error building chat message to send: " + e.getLocalizedMessage());
                }
                return null;
            };

            protected void onPostExecute(Void result) {
                hideProgressDialog();
            };
        }.execute();

    }

    public void onClick(android.view.View v) {
        switch (v.getId()){
            case R.id.tvLogo:
                // sendEncodedImage("TEST");
                break;
            default:
                break;
        }
    }

    private final class LongClickListener implements OnLongClickListener {

        @Override
        public boolean onLongClick(View view) {
            ClipData data = ClipData.newPlainText("", "");
            DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            return true;
        }

    }

    /*
     * USED FOR DRAG IMAGE INTO SMART TV BUCKET IMAGE
     */
    private class ImageDragListener implements OnDragListener {
        Drawable enterShape = getResources().getDrawable(R.drawable.smart_tv2);
        Drawable normalShape = getResources().getDrawable(R.drawable.smart_tv);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (event.getAction()){
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundDrawable(normalShape);
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    if (v.getId() == R.id.tvLogo) {
                        String imagePath = (String) ((ImageView) view).getTag();
                        sendEncodedImage(GalleryAdapter.getEncodedImage(imagePath));
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundDrawable(normalShape);
                default:
                    break;
            }
            return true;
        }
    }
}
