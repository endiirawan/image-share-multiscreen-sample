package com.srin.imagesharesample;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.samsung.multiscreen.application.Application;
import com.samsung.multiscreen.application.ApplicationAsyncResult;
import com.samsung.multiscreen.application.ApplicationError;
import com.samsung.multiscreen.channel.Channel;
import com.samsung.multiscreen.device.Device;
import com.samsung.multiscreen.device.DeviceAsyncResult;
import com.samsung.multiscreen.device.DeviceError;

public class AppLauncherActivity extends Activity {
    private static final String TAG = AppLauncherActivity.class.getSimpleName();
    private ProgressDialog progressDialog;
    private TextView deviceNameTextView;

    private Application app;

    private String runTitle = "ChatBinus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_launcher);

        deviceNameTextView = (TextView) findViewById(R.id.deviceNameTextView);
        Device.getDevice(URI.create(getIntent().getStringExtra(DevicePickerActivity.EXTRA_SELECTED_DEVICE_URI)),
            new DeviceAsyncResult<Device>() {

                @Override
                public void onResult(Device dev) {
                    Log.e("search", "Device connected: " + dev.getIPAddress());
                    DataManager.mDevice = dev;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            deviceNameTextView.setText(DataManager.mDevice.getName() + " - " + DataManager.mDevice.getIPAddress());

                        }
                    });

                    fetchApplicationState();
                }

                @Override
                public void onError(final DeviceError error) {
                    Log.e("search", "Search Failed : " + error.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AppLauncherActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

        Button launchApplicationButton = (Button) findViewById(R.id.launchApplicationButton);
        launchApplicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchApplication();
            }
        });

        Button connectToChannelButton = (Button) findViewById(R.id.connectToChannelButton);
        connectToChannelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectToDevice();
            }
        });
    }

    private void showProgressDialog(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                progressDialog = ProgressDialog.show(AppLauncherActivity.this, "", message, true);
            }
        });
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * CONNECT TO DEVICE
     */
    private void connectToDevice() {
        Log.d(TAG, "connectToChannel()");
        if (null == DataManager.mDevice) {
            Toast.makeText(this, "No Device Selected", Toast.LENGTH_SHORT).show();
            return;
        }

        // disconnectChannel();
        showProgressDialog("Connecting");

        // TODO : CONNECT TO DEVICE
        Map<String, String> clientAttributes = new HashMap<String, String>();
        clientAttributes.put("name", "Mr.X");
        DataManager.mDevice.connectToChannel(DataManager.CHANNEL_ID, clientAttributes, new DeviceAsyncResult<Channel>() {
            @Override
            public void onResult(final Channel channel) {
                DataManager.mChannel = channel;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(AppLauncherActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AppLauncherActivity.this, HomeActivity.class));
                    }
                });
            }

            @Override
            public void onError(final DeviceError error) {
                DataManager.mChannel = null;
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(AppLauncherActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this)
                .setMessage("Terminate App ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                        terminateApplication();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        }
        return true;
    }

    private void fetchApplicationState() {

        if (DataManager.mDevice == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AppLauncherActivity.this, "Device instance missing", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });

        } else {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final TextView applicationStatusTextView = (TextView) findViewById(R.id.applicationStatusTextView);
                    applicationStatusTextView.setText("Loading Status for " + runTitle);
                }
            });

            DataManager.mDevice.getApplication(runTitle,
                new DeviceAsyncResult<Application>() {
                    @Override
                    public void onResult(Application app) {
                        AppLauncherActivity.this.app = app;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgress();
                                Toast.makeText(AppLauncherActivity.this, "Loaded Application: " + runTitle, Toast.LENGTH_SHORT).show();
                                updateApplicationStateText();
                            }
                        });

                    }

                    @Override
                    public void onError(DeviceError e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(
                                    AppLauncherActivity.this,
                                    "Failed to Load Application: "
                                        + runTitle,
                                    Toast.LENGTH_SHORT);
                                toast.show();
                                finish();
                            }
                        });
                    }
                });
        }
    }

    private void terminateApplication(){
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("launcher", "android");
        app.terminate(new ApplicationAsyncResult<Boolean>() {
            @Override
            public void onResult(final Boolean result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        finish();
                    }
                });
            }

            @Override
            public void onError(ApplicationError e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        Toast.makeText(AppLauncherActivity.this, "Could not Terminate Application", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
    
    private void launchApplication() {
        if (DataManager.mDevice == null) {
            Toast toast = Toast.makeText(AppLauncherActivity.this, "No Device Selected", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        showProgress("Launching...");

        if (app == null) {
            Toast toast = Toast.makeText(AppLauncherActivity.this, "No Application Selected", Toast.LENGTH_SHORT);
            toast.show();
            finish();
            return;
        }

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("launcher", "android");
        app.launch(parameters, new ApplicationAsyncResult<Boolean>() {
            @Override
            public void onResult(final Boolean result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        String message = result ? "Application Launched!!!" : "Application Failed to Launch!!!";
                        Toast toast = Toast.makeText(AppLauncherActivity.this, message, Toast.LENGTH_SHORT);
                        toast.show();
                        updateApplicationStateText();
                    }
                });
            }

            @Override
            public void onError(ApplicationError e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        Toast toast = Toast.makeText(AppLauncherActivity.this,
                            "Could not launch Application",
                            Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
            }
        });
    }

    private void updateApplicationStateText() {
        if (app == null)
            return;

        TextView applicationStatusTextView = (TextView) findViewById(R.id.applicationStatusTextView);

        String state;
        switch (app.getLastKnownStatus()){
            case INSTALLABLE:
                state = "INSTALLABLE";
                break;
            case RUNNING:
                state = "RUNNING";
                break;
            case STOPPED:
            default:
                state = "STOPPED";
        }
        applicationStatusTextView.setText("Status for " + runTitle + ": "
            + state);

    }

    private void showProgress(String message) {
        hideProgress();
        progressDialog = ProgressDialog.show(this, "", message, true);
    }

    private void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
